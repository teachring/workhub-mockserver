var express = require('express')
var router = express.Router()
const DummyData = require('./dummy-data');

let lastUsedId = 100;

router.post('/:classId/classroom/ques/close',(req,res) => {
    const newQues = {...DummyData.QuesAnsList[1]}
    newQues.questionId = req.body.questionID;
    newQues.answerBody = req.body.answerBody;
    newQues.closedBy = 2
    newQues.status = 'CLOSED';
    newQues.closeDateTime = (new Date()).toISOString()
    res.send(newQues)
})

router.get('/:classId/classroom/questions',(req,res) => {
    res.send(DummyData.QuesAnsList)
})

router.post('/:classId/announcement',(req,res) => {
    res.send({
        announcementId: lastUsedId++,
        createdBy: 1,
        createDateTime: '2020-01-28T07:59:00+05:30',
        message: req.body.message
    })
})

router.get('/:classId/classroom/announcements',(req,res) => {
    res.send(DummyData.Announcements);
})


router.get('/:classId/enquiries',(req,res) => {
    console.log("Reg_Queries-Class_ID ",req.params.classId)
    res.send(DummyData.EducatorRegQueries)
})


router.post('/:classId/moderator/add',(req,res) => {
    res.send();
})

router.get('/:classId/learners',(req,res) => {
    res.send(DummyData.RegiseredLearners)
})


  


module.exports = router


