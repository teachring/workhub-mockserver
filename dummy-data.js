const ClassInfoList = [
	{
		classId: 1,
		organizerName: "SPIDER",
		title: "WEB DEV WORKSHOP",
		description: "Intro to frontend development",
		startDateTime: "2020-01-29T09:00:00+05:30",
		endDateTime: "2020-02-03T05:00:00+05:30",
		venue: "Orion, NIT Trichy",
		isPaid: true,
		feeAmt: 100,
		offlinePayAvl: true,
		regFormFields: null,
		regStartDateTime: "2020-01-18T00:00:00+05:30",
		regEndDateTime: "2020-01-28T23:59:00+05:30",
		agenda: "Come and learn web development",
		prerequirement: "Basics of Programming",
		details: "Certificates will be provided",
		faq: null,
		bgColor: "linear-gradient(to right, #4facfe 0%, #00f2fe 100%)",
		isDraft: false,
		createdBy: 1,
		UpdatedAt: "2020-01-17T23:59:00+05:30",
		moderatorList: [1]
	},
	{
		classId: 2,
		organizerName: "RMI NIT Trichy",
		title: "Advanced Robotics Workshop",
		description: "Intro to autonomous swarm robotics",
		startDateTime: "2020-02-05T09:00:00+05:30",
		endDateTime: "2020-02-10T17:00:00+05:30",
		venue: "Lecture hall Complex, NIT Trichy",
		isPaid: true,
		feeAmt: 100,
		offlinePayAvl: true,
		regFormFields: null,
		regStartDateTime: "2020-01-25T00:00:00+05:30",
		regEndDateTime: "2020-01-30T23:59:00+05:30",
		agenda: "Come and learn web development",
		prerequirement: "Basics of Programming",
		details: "Certificates will be provided",
		faq: null,
		bgColor: "linear-gradient(to right, #4facfe 0%, #00f2fe 100%)",
		isDraft: false,
		createdBy: 1,
		UpdatedAt: "2020-01-17T23:59:00+05:30",
		moderatorList: [1]
	},
	{
		classId: 3,
		organizerName: "DELTA Force",
		title: "Machine Learning WORKSHOP -1",
		description: "Intro to python programming",
		startDateTime: "2020-02-15T09:00:00+05:30",
		endDateTime: "2020-02-18T05:00:00+05:30",
		venue: "Computer Science Dept, NIT Trichy",
		isPaid: true,
		feeAmt: 100,
		offlinePayAvl: true,
		regFormFields: null,
		regStartDateTime: "2020-02-10T00:00:00+05:30",
		regEndDateTime: "2020-02-15T23:59:00+05:30",
		agenda: "Come and learn web development",
		prerequirement: "Basics of Programming",
		details: "Certificates will be provided",
		faq: null,
		bgColor: "linear-gradient(to right, #4facfe 0%, #00f2fe 100%)",
		isDraft: false,
		createdBy: 1,
		UpdatedAt: "2020-01-08T23:59:00+05:30",
		moderatorList: [1]
	},
	{
		classId: 4,
		organizerName: "SPIDER R&D",
		title: "Electronics and App-Dev Workshop",
		description: "How to build a autonomous car from scratch.",
		startDateTime: "2020-02-06T09:00:00+05:30",
		endDateTime: "2020-02-09T15:00:00+05:30",
		venue: "Orion Room S5, NIT Trichy",
		isPaid: true,
		feeAmt: 100,
		offlinePayAvl: true,
		regFormFields: null,
		regStartDateTime: "2020-01-30T00:00:00+05:30",
		regEndDateTime: "2020-02-03T23:59:00+05:30",
		agenda: "Come and learn web development",
		prerequirement: "Basics of Programming",
		details: "Certificates will be provided",
		faq: null,
		bgColor: "linear-gradient(to right, #4facfe 0%, #00f2fe 100%)",
		isDraft: false,
		createdBy: 1,
		UpdatedAt: "2020-01-17T23:59:00+05:30",
		moderatorList: [1]
	},
];

const EducatorClasses = [
	{
		classId: 1,
		organizerName: "SPIDER",
		title: "WEB DEV WORKSHOP",
		description: "Intro to frontend development",
		startDateTime: "2020-01-29T09:00:00+05:30",
		endDateTime: "2020-02-03T17:00:00+05:30",
		venue: "Orion, NIT Trichy",
		isPaid: true,
		feeAmt: 100,
		offlinePayAvl: true,
		regFormFields: null,
		regStartDateTime: "2020-01-18T00:00:00+05:30",
		regEndDateTime: "2020-01-28T23:59:00+05:30",
		agenda: "Come and learn web development",
		prerequirement: "Basics of Programming",
		details: "Certificates will be provided",
		faq: null,
		bgColor: "linear-gradient(to right, #4facfe 0%, #00f2fe 100%)",
		isDraft: false,
		createdBy: 1,
		UpdatedAt: "2020-01-17T23:59:00+05:30",
		moderatorList: [1]
	},
	{
		classId: 2,
		organizerName: "DELTA Force",
		title: "Machine Learning WORKSHOP",
		description: "Intro to python programming",
		startDateTime: "2020-01-20T09:00:00+05:30",
		endDateTime: "2020-01-23T05:00:00+05:30",
		venue: "Orion, NIT Trichy",
		isPaid: true,
		feeAmt: 100,
		offlinePayAvl: true,
		regFormFields: null,
		regStartDateTime: "2020-01-10T00:00:00+05:30",
		regEndDateTime: "2020-01-15T23:59:00+05:30",
		agenda: "Come and learn web development",
		prerequirement: "Basics of Programming",
		details: "Certificates will be provided",
		faq: null,
		bgColor: "linear-gradient(to right, #4facfe 0%, #00f2fe 100%)",
		isDraft: false,
		createdBy: 1,
		UpdatedAt: "2020-01-08T23:59:00+05:30",
		moderatorList: [1]
	},
	{
		classId: 3,
		organizerName: "SPIDER R&D",
		title: "Tronix Workshop",
		description: "Intro to frontend development",
		startDateTime: "2020-02-10T09:00:00+05:30",
		endDateTime: "2020-02-13T15:00:00+05:30",
		venue: "Orion, NIT Trichy",
		isPaid: true,
		feeAmt: 100,
		offlinePayAvl: true,
		regFormFields: null,
		regStartDateTime: "2020-01-18T00:00:00+05:30",
		regEndDateTime: "2020-01-31T23:59:00+05:30",
		agenda: "Come and learn web development",
		prerequirement: "Basics of Programming",
		details: "Certificates will be provided",
		faq: null,
		bgColor: "linear-gradient(to right, #4facfe 0%, #00f2fe 100%)",
		isDraft: false,
		createdBy: 1,
		UpdatedAt: "2020-01-17T23:59:00+05:30",
		moderatorList: [1]
	},
	{
		classId: 4,
		organizerName: "Spider R&D NIT Trichy",
		title: "Mobile Application Dev Workshop",
		isDraft: true,
		isPaid: true,
		createdBy: 1,
		UpdatedAt: "2020-01-17T23:59:00+05:30",
	}
]

const Notifications = [
	{
		notificationId: 1,
		classId: 1,
		createDateTime: "2020-01-17T23:59:00+05:30",
		payload:
			'{"enquiryId":20,"body":"Hello dear2","creationDateTime":"2020-01-29T10:53:10.658Z","status":"PENDING","createdBy":1}',
		isRead: false,
		notificationType: "REG_ENQUIRY"
	},
	{
		notificationId: 2,
		classId: 1,
		createDateTime: "2020-01-17T23:59:00+05:30",
		payload:
			'{"enquiryId":10,"body":"Hello dear","creationDateTime":"2020-01-29T10:53:10.658Z","status":"PENDING","createdBy":3}',
		isRead: false,
		notificationType: "REG_ENQUIRY"
	}
];

// enquiryId: number,
// body: string,
// creationDateTime: string,
// status: 'PENDING' | 'RESOLVED'
// createdBy: number

const RegData = [
	{
		regDateTime: "2020-01-19T01:59:00+05:30",
		paymentDetails: {
			isPaid: true,
			modeSelected: "ONLINE",
			paymentDateTime: "2020-01-19T01:59:00+05:30"
		}
	},
	{
		regDateTime: "2020-01-26T01:59:00+05:30",
		paymentDetails: {
			isPaid: true,
			modeSelected: "ONLINE",
			paymentDateTime: "2020-01-26T01:59:00+05:30"
		}
	}

];

const EducatorRegQueries = [
	{
		enquiryId: 1,
		body: "Will certificates be provided?",
		creationDateTime: "2020-01-19T01:59:00+05:30",
		status: "PENDING",
		createdBy: 1
	},
	{
		enquiryId: 2,
		body: "Is Laptop mandatory to attend the workshop?",
		creationDateTime: "2020-01-19T01:59:00+05:30",
		status: "RESOLVED",
		createdBy: 1
	},
	{
		enquiryId: 3,
		body: "Can I start after 3 days?",
		creationDateTime: "2020-01-17T01:59:00+05:30",
		status: "PENDING",
		createdBy: 1
	},
	{
		enquiryId: 1,
		body: "Will basics of C++ will be taught?",
		creationDateTime: "2020-01-20T01:59:00+05:30",
		status: "RESOLVED",
		createdBy: 1
	}
];

const UserList = [
	{
		id: 1,
		userName: "Udit Agarwal",
		phoneNumber: "+91-9790488941"
	},
	{
		id: 2,
		userName: "Vaibhav Rockstar",
		phoneNumber: "+91 9499898989"
	},
	{
		id: 3,
		userName: "Shreehari A",
		phoneNumber: "+91 9898989898"
	}
];

const LearnerRegQueries = [
	{
		enquiryId: 1,
		body: "Will certificates be provided?",
		creationDateTime: "2020-01-19T01:59:00+05:30"
	},
	{
		enquiryId: 2,
		body: "Is Laptop mandatory to attend the workshop?",
		creationDateTime: "2020-01-19T01:59:00+05:30"
	},
	{
		enquiryId: 3,
		body: "Can I start after 3 days?",
		creationDateTime: "2020-01-17T01:59:00+05:30"
	},
	{
		enquiryId: 1,
		body: "Will basics of C++ will be taught?",
		creationDateTime: "2020-01-20T01:59:00+05:30"
	}
];

const QuesAnsList = [
	{
		questionId: 1,
		askedBy: 1,
		questionBody:
			"I do not understand React hooks. Can you please explain it to me again?",
		askedDateTime: "2020-01-20T01:59:00+05:30",
		answerBody: "Sure, Come to class early tomorrow.",
		closedBy: 1,
		closeDateTime: "2020-01-20T04:59:00+05:30",
		status: "CLOSED"
	},
	{
		questionId: 2,
		askedBy: 2,
		questionBody: "I do not understand anything in web dev, what to do?",
		askedDateTime: "2020-01-20T02:59:00+05:30",
		status: "PENDING"
	},
	{
		questionId: 3,
		askedBy: 1,
		questionBody: "sapdiya?",
		askedDateTime: "2020-01-20T01:59:00+05:30",
		answerBody: "sapten.",
		closedBy: 1,
		closeDateTime: "2020-01-20T04:59:00+05:30",
		status: "CLOSED"
	},
	{
		questionId: 4,
		askedBy: 1,
		questionBody: "enna pandra?",
		askedDateTime: "2020-01-20T01:59:00+05:30",
		status: "PENDING"
	},
	{
		questionId: 5,
		askedBy: 1,
		questionBody: "React puriyavillai",
		askedDateTime: "2020-01-20T01:59:00+05:30",
		answerBody: "naan same",
		closedBy: 1,
		closeDateTime: "2020-01-20T04:59:00+05:30",
		status: "CLOSED"
	}
];

// export type RegFormQuestion = {
//     qid: number,
//     question: string,
//     type: RegFormQuesType,
//     options?: Array<RegFormQuesOption>,
//     required: boolean,
//     selectedOption?: string,
//     answer?: string

//   }

const RegiseredLearners = [
	{
		userID: 1,
		regDateTime: "2020-01-20T01:59:00+05:30",
		regFormData:
			'{"1":{"qid":1,"question":"Roll no","type":"text","required":false,"answer":"106115089"},"2":{"qid":2,"question":"Department","type":"radio","options":[{"text":"CSE","selected":true},{"text":"ECE","selected":false}],"required":true,"selectedOption":"CSE"}}',
		paymentDetails: {
			modeSelected: "ONLINE",
			isPaid: true,
			paymentDateTime: "2020-01-20T01:59:00+05:30"
		}
	},
	{
		userID: 2,
		regDateTime: "2020-01-25T01:59:00+05:30",
		paymentDetails: {
			modeSelected: "OFFLINE",
			isPaid: false,
			lastReminderDateTime: "2020-01-28T07:59:00+05:30"
		}
	}
];

const Announcements = [
	{
		announcementId: 1,
		createdBy: 1,
		createDateTime: "2020-01-28T07:59:00+05:30",
		message: "Class Shifted to Orion S4"
	},
	{
		announcementId: 2,
		createdBy: 2,
		createDateTime: "2020-01-28T07:59:00+05:30",
		message:
			"Please bring a extension board along with your laptop and Charger."
	}
];

module.exports = {
	ClassInfoList,
	RegData,
	Notifications,
	UserList,
	EducatorRegQueries,
	LearnerRegQueries,
	QuesAnsList,
	RegiseredLearners,
	Announcements,
	EducatorClasses
};
