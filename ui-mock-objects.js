
const data: types.CreateClassForm = {
    organizerName: "Spider",
    title: "React.js workshop",
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Est, minus?",
    startDateTime: new Date(),
    endDateTime: new Date(),
    venue: "Orion G11",
    isPaid: false,
    feeAmt: 100,
    offlinePayAvl: true,
    regFormFields: regFormFields,
    regStartDateTime: new Date(),
    regEndDateTime: new Date(),
    agenda: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptate, totam?",
    prerequirement: "<h3>Sample prerequirement</h3>",
    details: "<h2>Sample details</h2>",
    faq: [
        { q: "Que 1", a: "Ans 1" },
        { q: "Que 2", a: "Ans 2" }
    ],
    classId: 123
}