const express = require("express");
var cookieParser = require('cookie-parser')
const bodyParser = require("body-parser");

const DummyData = require('./dummy-data');
const EducatorClass = require('./educator-classroom');
const PaymentRoutes = require('./rzp-payments');

const app = express()
const port = 7000

app.use(bodyParser.json())
app.use(cookieParser())

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
  // Request methods you wish to allow
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});


var lastUsedId = 400;



app.get('/classes/upcoming',(req, res) => {
  var resArr = [];
  resArr = DummyData.ClassInfoList.slice(2,);
  res.send(resArr)

})


app.get('/classes/registered',(req, res) => {
  var resArr = [];
  DummyData.RegData.forEach((regObj,idx) => {
      resArr.push({classInfo: DummyData.ClassInfoList[idx],regData: regObj})
  })
  res.send(resArr)
})



app.get('/user/notifications/poll',(req,res) => {
  console.log("Last Id" ,req.query.lastId)
  res.send(DummyData.Notifications.slice(req.query.lastId,))
})


app.get('/user/notifications',(req,res) => {
  res.send(DummyData.Notifications.slice(0,1))
})

const cookieExpiryTime = 30*60*1000;

app.post('/user/create',(req,res) => {

  console.log("Signup Request Received ",req.body);

  res.cookie('authToken',"secret_token_for_auth", { maxAge: cookieExpiryTime});
  res.cookie('userId',1,{maxAge:cookieExpiryTime});
  res.cookie('userName','Udit Agarwal',{maxAge: cookieExpiryTime})
  res.send()
})

app.post('/login',(req,res) => {
  res.cookie('authToken',"secret_token_for_auth", { maxAge: cookieExpiryTime});
  res.cookie('userId',1,{maxAge: cookieExpiryTime});
  res.cookie('userName','Udit Agarwal',{maxAge: cookieExpiryTime})
  res.send()
})

app.post('/user/isregistered', (req, res) => {
  if(req.body.phoneNumber.startsWith('+91-')){
    res.send({result:true})
  } else {
    res.send({result:false})
  }
})

app.post('/educator/users/fetchdetails',(req,res) => {
  console.log("UserRequest Query ",req.body)
  resArr = [];
  DummyData.UserList.forEach((user) => {
    if(req.body.includes(user.id))
      resArr.push(user);
  })
  res.send(resArr);
})

app.use('/educator/class',EducatorClass)

app.use('/payments',PaymentRoutes);

app.get('/class/:classId/enquiries',(req,res) => {
  res.send(DummyData.LearnerRegQueries);
})

app.post('/class/:classId/enquiries',(req,res) => {
  
  res.send({enquiryId:getRandomInt(10,3000),body:req.body.newQuery,creationDateTime:(new Date()).toISOString()})
})

app.get('/class/:classId/classroom/token',(req,res) => {
  res.send({token:'learner-access-token'})
})

app.get('/class/:classId/classroom/questions',(req,res) => {
  res.send(DummyData.QuesAnsList);
})

app.post('/class/:classId/classroom/ques/create',(req,res) => {
  res.send({
    questionId:lastUsedId++,
    askedBy: 1,
    questionBody: req.body.quesBody,
    askedDateTime: (new Date()).toISOString(),
	  status:'PENDING'
  })
})

app.get('/educator/findmoderators',(req,res) =>{
  if(req.query.phone == '9488251214'){
    res.send(DummyData.UserList[1]);
  } else{
    res.send();
  }
})

app.get('/educator/classes',(req,res) => {
  res.send(DummyData.EducatorClasses);
})

app.post('/user/notifications/read',(req,res) => {
  res.send();
})

app.use(function (err, req, res, next) {
    console.error(err.stack)
    res.status(500).send('Something broke!')
})


app.listen(port, () => console.log(`Example app listening on port ${port}!`))


function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

// MOCK SERVER